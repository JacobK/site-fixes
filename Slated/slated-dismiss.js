/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Copyright (C) 2022 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
	identifier: slated-dismiss
	long name: Dismiss for Slated
	description: automatically dismiss the warning saying you need JavaScript
	URL patterns: */ // TODO: fix match for the page on archive.org
//		https://slated.org/***
//		https://web.archive.org/web/*/*//slated.org/***
/*	example URLs: */
//		http://slated.org/windows_by_stealth_the_updates_you_dont_want
//		https://web.archive.org/web/20200219180230/http://slated.org/windows_by_stealth_the_updates_you_dont_want


/* force show the body element in case the CSS inside the noscript element does
	not show it (I think most browsers will not parse HTML inside noscript
	elements.) */
document.body.style = "display: block !important;";

// remove element with id "seckit-noscript-tag"
/* TODO: make it so the user has to dismiss the warning, since the website is
	explicitly saying you need JavaScript, and we don't know why. */
/* TODO: make this part run after NoScript replaces the noscript element with a
	span element, otherwise the code will not remove the element and then the
	element will appear. Wrapping the code in
	`document.addEventListener("DOMContentLoaded", function() {` ... `});` or
	`document.addEventListener('readystatechange', event => {
		if (event.target.readyState === "complete") {` ... `}});` (idea from
	https://stackoverflow.com/a/32867110) does not seem to guarantee the code
	is run after NoScript. */
if (document.getElementById("seckit-noscript-tag")) { /* conditional because
		the element only appears when noscript elements are parsed */
	document.getElementById("seckit-noscript-tag").remove();
} else {
	// This will probably get lost among all the other warnings :P
	console.warn(`If you have NoScript installed, you may need to check "script" for this page or uncheck "noscript" for this page, in order for the page to display properly.`);
}
