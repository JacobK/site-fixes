mkdir build
hydrilla-builder -s Anbox/ -d build
hydrilla-builder -s Ars/ -d build
hydrilla-builder -s Grammarly/ -d build
hydrilla-builder -s 'Information Week'/ -d build
hydrilla-builder -s Lifewire/ -d build
hydrilla-builder -s LineageOS/  -d build
hydrilla-builder -s 'Mojang Jira'/ -d build
hydrilla-builder -s Nintendo/  -d build
hydrilla-builder -s OpenGameArt/  -d build
hydrilla-builder -s Slated/  -d build
hydrilla-builder -s TengoInternet/  -d build
hydrilla-builder -s 'The Street'/ -d build
hydrilla-builder -s 'tp link'/ -d build
hydrilla-builder -s 'Via News'/ -d build
hydrilla-builder -s wikidevi/  -d build
